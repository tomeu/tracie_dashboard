# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab
import re
from tracie_dashboard.settings import GITLAB_TOKEN
from gitlab.exceptions import GitlabGetError

gl = None

GITLAB_URL = "http://gitlab.freedesktop.org"

def _connect_to_ci(project_path):
    global gl
    if gl is None:
        gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
    return gl.projects.get(project_path)

def _get_job_trace_results(trace):
    results = {}

    result_pattern = re.compile("\[check_image\] Images (\S+) for (\S+)")
    for l in trace.splitlines():
        m = result_pattern.match(l)
        if m is not None:
            results[m[2]] = m[1]

    return results

class Job:
    __slots__ = ('id', 'project_path', 'pipeline_id', 'status', 'timestamp', 'device',
                 'trace_results')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.pipeline_id = None
        self.status = None
        self.device = None
        self.timestamp = None
        self.trace_results = {}

    def url(self):
        return GITLAB_URL + "/" + self.project_path + "/-/jobs/" + str(self.id)

    def trace_summary_url(self, name):
        return self.url()

class Pipeline:
    __slots__ = ('id', 'project_path', 'status', 'mesa_repo', 'mesa_sha',
                 'test_job_ids')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.status = None
        self.mesa_repo = None
        self.mesa_sha = None
        self.test_job_ids = []

    def url(self):
        return GITLAB_URL + "/" + self.project_path + "/pipelines/" + str(self.id)

def get_pipeline_ids(project_path):
    project = _connect_to_ci(project_path)

    return [p.id for p in project.pipelines.list()]

def get_pipeline(project_path, pid):
    project = _connect_to_ci(project_path)

    p = project.pipelines.get(pid)

    pipeline = Pipeline()
    pipeline.id = p.id
    pipeline.project_path = project_path
    pipeline.status = p.status
    pipeline.mesa_repo = "https://gitlab.freedesktop.org/%s" % project_path
    pipeline.mesa_sha = p.sha

    for j in p.jobs.list(all=True):
        if "-traces" in j.name:
            pipeline.test_job_ids.append(j.id)

    return pipeline

def get_test_job(project_path, jid):
    project = _connect_to_ci(project_path)

    job = project.jobs.get(jid)
    trace = job.trace().decode("utf-8")

    test_job = Job()
    test_job.id = jid
    test_job.project_path = project_path
    test_job.pipeline_id = job.pipeline['id']
    test_job.status = job.status
    # TODO: Use a better way to figure out the device
    test_job.device = job.name.replace("-traces", "")
    test_job.trace_results = _get_job_trace_results(trace)
    test_job.timestamp = job.finished_at

    return test_job
