# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from django.test import TestCase, Client
from dashboard.models import PipelineResult, TraceResult, PerDeviceResults
from dashboard.views import ci
import dashboard.cache as cache

from threading import Thread
from time import sleep
import unittest

class FakeCI:
    def __init__(self):
        self.pipelines = []
        self.jobs = []
        self.time_to_connect = 0.0

        # Monkey patch ci module used by dashboard.views
        ci.get_pipeline_ids = self.get_pipeline_ids_impl
        ci.get_pipeline = self.get_pipeline_impl
        ci.get_test_job = self.get_test_job_impl

    def add_pipeline(self, pid, status, test_job_ids):
        assert(pid < 256 and pid > 0)
        p = ci.Pipeline()
        p.id = pid;
        p.project_path = "fakeci";
        p.status = status
        p.mesa_sha = "0a1b2c3d" + hex(pid)[2:] + "4e5f"
        p.mesa_repo = "https://example.com/mesa/mesa"
        p.test_job_ids = test_job_ids

        self.pipelines.append(p)

    def add_job(self, jid, pid, status, trace_results):
        j = ci.Job()
        j.id = jid
        j.project_path = "fakeci";
        j.pipeline_id = pid
        j.status = status
        j.device = "device-%d" % jid
        j.trace_results = trace_results
        j.timestamp = jid
        self.jobs.append(j)

    def get_pipeline_ids_impl(self, project_path):
        return [p.id for p in self.pipelines if p.project_path == project_path]

    def get_pipeline_impl(self, project_path, pid):
        if self.time_to_connect > 0:
            sleep(self.time_to_connect)
        return next(p for p in self.pipelines if p.id == pid and p.project_path == project_path)

    def get_test_job_impl(self, project_path, jid):
        if self.time_to_connect > 0:
            sleep(self.time_to_connect)
        return next(j for j in self.jobs if j.id == jid and j.project_path == project_path)

    def get_pipeline_ids(self):
        return self.get_pipeline_ids_impl("fakeci")

    def get_pipeline(self, pid):
        return self.get_pipeline_impl("fakeci", pid)

    def get_test_job(self, jid):
        return self.get_test_job_impl("fakeci", jid)

class DashboardTestCase(TestCase):
    def _assertLandingContainsPipeline(self, response, pipeline):
        job_links = ["/dashboard/job/fakeci/" + str(jid) + "/" for jid in pipeline.test_job_ids]
        for job_link in job_links:
            self.assertContains(response, job_link)
        self.assertContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertContains(response, pipeline.url())

    def _assertLandingNotContainsPipeline(self, response, pipeline):
        job_links = ["/dashboard/job/fakeci/" + str(jid) + "/" for jid in pipeline.test_job_ids]
        for job_link in job_links:
            self.assertNotContains(response, job_link)
        self.assertNotContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertNotContains(response, pipeline.url())

    def _assertDatabaseContainsPipeline(self, pipeline):
        pr = PipelineResult.objects.get(id=pipeline.id)
        self.assertEquals(pr.mesa_sha, pipeline.mesa_sha)
        self.assertEquals(
            pr.status,
            cache._ci_to_result_status_map.get(pipeline.status, "unknown")
        )

    def setUp(self):
        self.fake_ci = FakeCI()

        self.fake_ci.add_job(13, 1, "success", {"trace1": "match", "trace2": "match"})
        self.fake_ci.add_job(14, 2, "failed", {"trace1": "match", "trace2": "differ"})
        self.fake_ci.add_job(15, 4, "success", {"trace1": "match", "trace2": "match"})
        self.fake_ci.add_job(16, 5, "success", {})
        self.fake_ci.add_job(17, 5, "failed", {})

        self.fake_ci.add_pipeline(1, "success", [13])
        self.fake_ci.add_pipeline(2, "failed", [14])
        self.fake_ci.add_pipeline(3, "failed", [])
        self.fake_ci.add_pipeline(4, "success", [15])
        self.fake_ci.add_pipeline(5, "failed", [16, 17])

        self.client = Client()

    def test_db_is_populated_from_ci(self):
        self.fake_ci.time_to_connect = 0.05

        self.client.get('/dashboard/project/fakeci/')

        for p in self.fake_ci.pipelines:
            self._assertDatabaseContainsPipeline(p)

    def test_landing_contains_only_finished_pipelines(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})

        response = self.client.get('/dashboard/project/fakeci/')

        p = self.fake_ci.get_pipeline(100)
        self._assertLandingNotContainsPipeline(response, p)

    def test_landing_contains_job_status(self):
        shown_jobs = []
        for p in self.fake_ci.pipelines:
            for jid in p.test_job_ids:
                shown_jobs.append(self.fake_ci.get_test_job(jid))

        count_pass = len([j for j in shown_jobs if j.status == "success"])
        count_fail = len([j for j in shown_jobs if j.status == "failed"])

        response = self.client.get('/dashboard/project/fakeci/')

        self.assertContains(response, "pass", count=2 * count_pass)
        self.assertContains(response, "fail", count=2 * count_fail)

    def test_landing_contains_job_device(self):
        response = self.client.get('/dashboard/project/fakeci/')

        for p in self.fake_ci.pipelines:
            for jid in p.test_job_ids:
                self.assertContains(response, "device-%d" % jid, count = 1)

    def test_job_view_contains_trace_names_and_links(self):
        response = self.client.get('/dashboard/job/fakeci/13/')
        job = self.fake_ci.get_test_job(13)

        self.assertContains(response, "trace1")
        self.assertContains(response, job.trace_summary_url("trace1"))
        self.assertContains(response, "trace2")
        self.assertContains(response, job.trace_summary_url("trace2"))

    def test_job_view_contains_trace_status(self):
        response = self.client.get('/dashboard/job/fakeci/13/')
        self.assertContains(response, "pass", count=3) # 2 traces + summary
        self.assertNotContains(response, "fail")

        response = self.client.get('/dashboard/job/fakeci/14/')
        self.assertContains(response, "pass", count=1)
        self.assertContains(response, "fail", count=2) # 1 trace + summary

    def test_job_view_contains_job_info(self):
        pipeline = self.fake_ci.get_pipeline(5)
        job16 = self.fake_ci.get_test_job(16)
        response = self.client.get('/dashboard/job/fakeci/16/')

        self.assertContains(response, "device-16")
        self.assertContains(response, job16.url())
        self.assertContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertContains(response, pipeline.url())

    def test_job_view_for_running_job_contains_no_results(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})

        response = self.client.get('/dashboard/job/fakeci/16/')
        self.assertContains(response, "pass", count=1) # summary = pass
        self.assertNotContains(response, "fail")

    @unittest.skip("We don't properly handle requests in multiple threads")
    def test_job_view_parallel_fetch_trace_status_no_duplicates(self):
        self.fake_ci.time_to_connect = 0.01
        self.fake_ci.add_job(200, 201, "success",
                             {"trace1": "match",
                              "trace2": "match",
                              "trace3": "match",
                              "trace4": "match",
                              "trace5": "match",
                              "trace6": "match", })
        self.fake_ci.add_pipeline(201, "success", [200])

        def fetcher(client):
            response = client.get('/dashboard/job/fakeci/200/')

        threads = []
        for i in range(10):
            threads.append(Thread(target=fetcher, args=(self.client,)))
            threads[-1].start()

        for t in threads:
            t.join()

        response = self.client.get('/dashboard/job/fakeci/200/')
        self.assertContains(response, "pass", count=7) # 6 traces + summary

        self.fake_ci.time_to_connect = 0.0

    def test_database_is_updated_with_final_ci_results(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})
        p = self.fake_ci.get_pipeline(100)

        self.client.get('/dashboard/project/fakeci/')
        self._assertDatabaseContainsPipeline(p)

        p.status = "success"
        self.fake_ci.get_test_job(200).status = "success"

        # Database should include updated pipeline info
        self.client.get('/dashboard/project/fakeci/')
        self._assertDatabaseContainsPipeline(p)

    def test_namespaced_project_path_is_recognized(self):
        self.fake_ci.add_pipeline(100, "success", [200])
        self.fake_ci.add_job(200, 100, "success", {})
        p = self.fake_ci.get_pipeline(100)
        j = self.fake_ci.get_test_job(200)
        p.project_path = "namespace/project";
        j.project_path = "namespace/project";

        self.assertIn("namespace/project", p.url())
        self.assertIn("namespace/project", j.url())

        self.client.get('/dashboard/job/namespace/project/200/')
        self._assertDatabaseContainsPipeline(p)

    def test_db_is_populated_with_per_device_results(self):
        self.client.get('/dashboard/devices/fakeci/')
        for j in self.fake_ci.jobs:
            pdr = PerDeviceResults.objects.filter(id="fakeci:%s" % j.device)[0]
            self.assertEqual(pdr.name, j.device)
            self.assertEqual(pdr.last_run.id, j.id)
            if j.status == "success":
                self.assertEqual(pdr.last_good_run.id, j.id)
                self.assertEqual(pdr.first_bad_run, None)
            else:
                self.assertEqual(pdr.last_good_run, None)
                self.assertEqual(pdr.first_bad_run.id, j.id)

    def test_devices_view_contains_info(self):
        response = self.client.get('/dashboard/devices/fakeci/')

        # Each device causes the sha to be displayed in 2 columns (either
        # current+failing, or current+pass) and the 2 respective urls
        # (each job corresponds to a different device in this setup).
        for p in self.fake_ci.pipelines:
            self.assertContains(response, p.mesa_sha[:10] + "\n", count=2 * len(p.test_job_ids))
            self.assertContains(response, p.mesa_sha, count=2 * len(p.test_job_ids))

        count_pass = len([j for j in self.fake_ci.jobs if j.status == "success"])
        count_fail = len([j for j in self.fake_ci.jobs if j.status == "failed"])

        self.assertContains(response, "all good", count=count_pass)
        self.assertContains(response, "no good runs yet", count=count_fail)

    def test_devices_view_is_populated_per_project(self):
        self.fake_ci.add_pipeline(100, "success", [200])
        self.fake_ci.add_job(200, 100, "success", {})
        p = self.fake_ci.get_pipeline(100)
        j = self.fake_ci.get_test_job(200)
        p.project_path = "namespace/project";
        j.project_path = "namespace/project";

        response = self.client.get('/dashboard/devices/fakeci/')

        for j in (j for j in self.fake_ci.jobs if j.project_path == "fakeci"):
            self.assertContains(response, j.device)

        for j in (j for j in self.fake_ci.jobs if j.project_path != "fakeci"):
            self.assertNotContains(response, j.device)
