# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from django.db import models

class PipelineResult(models.Model):
    id = models.IntegerField(primary_key=True)
    project_path = models.CharField(max_length=200)
    url = models.URLField()
    mesa_sha = models.CharField(max_length=40, null=True)
    mesa_url = models.URLField(null=True)
    status = models.CharField(max_length=10)

class JobResult(models.Model):
    id = models.IntegerField(primary_key=True)
    project_path = models.CharField(max_length=200)
    url = models.URLField()
    status = models.CharField(max_length=10)
    device = models.CharField(max_length=100)
    timestamp = models.CharField(max_length=30)
    pipeline_result = models.ForeignKey(PipelineResult, on_delete=models.CASCADE,
                                        related_name="job_results")

class TraceResult(models.Model):
    summary_url = models.URLField()
    status = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    job_result = models.ForeignKey(JobResult, on_delete=models.CASCADE,
                                   related_name="trace_results")

class PerDeviceResults(models.Model):
    id = models.CharField(primary_key=True, max_length=200)
    name = models.CharField(max_length=200)
    last_good_run = models.ForeignKey(JobResult, on_delete=models.CASCADE,
                                   related_name="last_good_run", null=True)
    first_bad_run = models.ForeignKey(JobResult, on_delete=models.CASCADE,
                                   related_name="first_bad_run", null=True)
    last_run = models.ForeignKey(JobResult, on_delete=models.CASCADE,
                                   related_name="last_run")
